import re

fp = open("data/seg_output.txt")
content = fp.read()
fp.close()

bow = {}

for phrase in content.split():
	try:
		bow[phrase] += 1
	except KeyError:
		bow[phrase] = 1

fp = open("data/bow_output.txt", "w")
f1 = open("data/stopword_1_output.txt", "w")
f2 = open("data/stopword_4_output.txt", "w")
#fp.write("\n".join(bow.keys()))
for word in bow:
	if len(word) <= 1:
		f1.write(word + "\n")
	elif re.match(r"[a-zA-Z]+", word):
                fp.write(word + "\n")
	elif re.match(r"^[0-9].+", word):
		f2.write(word + "\n")
	elif len(word) >= 4:
		f2.write(word + "\n")
	else:
		fp.write(word + "\n")
fp.close()
f1.close()
f2.close()


