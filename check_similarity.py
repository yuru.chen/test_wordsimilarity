import sys
import getopt
from gensim.models import word2vec

def _parsing_command_argument(argv):
	"""commandline arguments
	"""
	_HELP_TEXT = "python check_similarity.py [word1] [word2]"
	try:
		opts, args = getopt.getopt(argv, "h",("help"))
		for opt, arg in opts:
			if opt in ('--help','-h'):
				print(_HELP_TEXT)
				sys.exit(1)
		if len(args) == 2:
			input_str1 = args[0]
			input_str2 = args[1]
		else:
			print(_HELP_TEXT)
			sys.exit(1)
	except Exception as e:
		sys.stderr.write("Argument Error: %r " % (e,))
		sys.exit(1)
	return (input_str1, input_str2)

def getSimilarity(word1, word2):
	model = word2vec.Word2Vec.load("word2vec_window5.model")
	a = model.similarity(word2, word1)
	return a

def main():
	word1, word2 = _parsing_command_argument(sys.argv[1:])
	a = getSimilarity(word1, word2)
	print(word1, word2, a)

if __name__ == "__main__":
    main()
