import sys
from gensim.corpora import WikiCorpus
import jieba

def set_jieba():
	jieba.set_dictionary('jieba_dict/dict.txt.big')
	jieba.load_userdict("data/seg_dict.txt")

def get_stopword_set():
	stopword_set = set()
	with open("data/stopword.txt") as stopwords:
		for stopword in stopwords:
			stopword_set.add(stopword.strip("\n"))
	return stopword_set

def wiki_to_txt(argvs, stopword_set):
	file_path = argvs[0]
	wiki_corpus = WikiCorpus(file_path, dictionary={})
	texts_num = 0
	seg_output = open("data/seg_output.txt", "w", encoding="utf-8")
	with open("data/wiki_texts.txt",'w',encoding='utf-8') as output:
		for text in wiki_corpus.get_texts():
			texts_num += 1
			output.write(' '.join(text) + '\n')
			for sentence in text:
				words = jieba.cut(sentence, cut_all=False)
				for word in words:
					if word not in stopword_set:
						seg_output.write(word + ' ')
				seg_output.write("\n")
			seg_output.write("\n")
			if texts_num % 10000 == 0:
				print("%d text done" % texts_num)
	seg_output.close()
		

if __name__ == "__main__":
	set_jieba()
	print("Done set_jieba()")
	stopword_set = get_stopword_set()
	print("Donw get stopword set")
	wiki_to_txt(sys.argv[1:], stopword_set)

