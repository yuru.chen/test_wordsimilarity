# test_WordSimilarity

wiki_to_txt_and_seg.py

	- Download WikiCorpus, segment by jieba (user_dict, stopword for manually tuning)
	- Output data/seg_output.txt

seg_to_bow.py

	- Convert segmented text to bow (bag of words) for gensim
	- Filter out word with characters length <=1 & >=4
	- Output data/bow_output.txt 

gensim_train.py

	- Train similarity model (gensim)
	- Save to word2vec_window5.model

check_similarity.py

	- python check_similarity.py [word1] [word2]
	- Load saved model and output similarity score

