
from gensim.models import word2vec

def main():

	sentences = word2vec.LineSentence("data/seg_output.txt")
	model = word2vec.Word2Vec(
		sentences, 
		size=500,
		alpha=0.025,
		window=5,
		min_count=5, 
		max_vocab_size=None, 
		sample=0.001, 
		seed=1, 
		workers=3, 
		min_alpha=0.0001, 
		sg=0, 
		hs=0, 
		negative=5, 
		cbow_mean=1,
		iter=5, 
		null_word=0, 
		trim_rule=None, 
		sorted_vocab=1, 
		batch_words=10000
		)

	model.save("word2vec_window5.model")

	

if __name__ == "__main__":
    main()

